const fs = require('fs');
const inquirer = require('inquirer');

const pkg = require('./package.json');
const cliConfig = require('./webpack/utils/findCliConfig.js');

const merge = require('webpack-merge');

const questions = [
	{
		type: 'input',
		name: 'projectName',
		message: 'What is the name of the project ?',
		validate(inputValue) {
			let packageNameTest = /^(?:@[a-z0-9-~][a-z0-9-._~]*\/)?[a-z0-9-~][a-z0-9-._~]*$/;
			let valid = packageNameTest.test(inputValue);

			return (
				!!valid ||
				'The name of the project must match the pattern. See https://docs.npmjs.com/files/package.json for rules.'
			);
		},
	},
	{
		type: 'list',
		name: 'bootstrapVersion',
		message: 'Which version of Twitter Bootstrap will project be using?',
		choices: [
			{
				name: '3.3.7 (Legacy)',
				value: 'legacy',
			},
			{
				name: `${pkg.dependencies.bootstrap} (Modern)`,
				value: 'modern',
			},
		],
	},
	{
		type: 'list',
		name: 'projectType',
		message: 'What type of project are you creating ?',
		choices: [
			{
				name: 'Static HTML with jQuery',
				value: 'static',
			},
			{
				name: 'HTML components for Angular, React.js',
				value: 'components',
			},
			{
				name: 'Vue.js',
				value: 'vue',
			},
			{
				name: 'Liferay Theme',
				value: 'liferay',
			},
		],
	},
	{
		type: 'list',
		name: 'supportedBrowsers',
		message: 'Does the project need to support other browsers than specified ?',
		choices: [
			{
				name: 'No',
				value: 'modern',
			},
			{
				name: 'Old',
				value: 'old',
			},
			{
				name: 'Ancient',
				value: 'ancient',
			},
		],
	},
];

const updatePkg = (origPkg, projectName) => {
	origPkg = {
		...origPkg,
		name: projectName,
	};

	const pkgJSON = JSON.stringify(origPkg, null, 2);

	fs.writeFile('package.json', pkgJSON, 'utf8', function(err) {
		if (err) console.log(err);
	});
};

const createConfigFile = arr => {
	return new Promise((resolve, reject) => {
		const answersJSON = JSON.stringify(arr, null, 2);

		fs.writeFile('cli.config.json', answersJSON, 'utf8', function(err) {
			if (err) console.log(err);
			console.log('Happy coding!');
			resolve();
		});
	});
};

const serverStart = () => {
	const webpack = require('webpack');
	const WebpackDevServer = require('webpack-dev-server');
	const baseConfig = require('./webpack/configs/base.config.js');
	const devConfig = require('./webpack/configs/dev.config.js');
	const settings = require('./webpack/webpack.settings.js');

	const server = new WebpackDevServer(webpack(merge(baseConfig, devConfig)));

	server.listen(settings.devServerConfig.port, settings.devServerConfig.host, err => {
		if (err) throw err;
	});
};

const setupConfig = () => {
	inquirer.prompt(questions).then(answers => {
		updatePkg(pkg, answers.projectName);
		createConfigFile(answers).then(() => serverStart());
	});
};

const confirmRewriteConfig = () => {
	inquirer
		.prompt({
			type: 'confirm',
			name: 'rewriteConfig',
			message:
				'Existing config file found, this action will erase current config and create new config (prone to error), are you sure to continue ?',
		})
		.then(answers => {
			!answers.rewriteConfig ? serverStart() : setupConfig();
		});
};

if (cliConfig !== null) {
	confirmRewriteConfig();
} else {
	setupConfig();
}
