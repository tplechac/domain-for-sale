const path = require('path');
const p = x => path.resolve(__dirname, '..', x);

module.exports = {
	name: 'Example Project',
	copyright: 'Example Company, Inc.',
	paths: {
		src: {
			base: p('./src/'),
			scss: p('./src/scss/'),
			js: p('./src/js/'),
			pages: p('./src/static/pages/'),
			assets: p('./src/assets/')
		},
		dist: {
			base: p('./public/')
		},
	},
	devServerConfig: {
		public: 'http://localhost:8080',
		host: 'localhost',
		poll: false,
		port: 8080,
		https: false,
	},
};
