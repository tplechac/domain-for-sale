const pkg = require('../../package.json');
const cliConfig = require('../../cli.config.json');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');

// Configure the Postcss loader
const cssLoader = (isProduction = false) => {
	return {
		test: /\.(scss)$/,
		use: [
			MiniCssExtractPlugin.loader,
			{
				loader: 'css-loader',
				options: {
					importLoaders: 4,
					sourceMap: !isProduction,
				},
			},
			{
				loader: 'resolve-url-loader',
			},
			{
				loader: 'icon-font-loader'
			},
			{
				loader: 'postcss-loader',
				options: {
					ident: 'postcss',
					sourceMap: !isProduction,
					plugins: () => [autoprefixer({ browsers: pkg.browserslist[cliConfig.supportedBrowsers] })],
				},
			},
			{
				loader: 'sass-loader',
				options: {
					sourceMap: !isProduction,
					outputStyle: 'compressed',
				},
			}
		],
	};
};

module.exports = {
	cssLoader: cssLoader,
};
