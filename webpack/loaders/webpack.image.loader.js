const path = require('path');

const imagemin = require('imagemin');

// Configure Image loader
const imageLoader = (isProduction = false) => {
	let devConfig = {
		test: /\.(png|jpe?g|gif|svg|webp)$/i,
		use: [
			{
				loader: 'file-loader',
				options: {
					name: 'assets/img/[name].[hash].[ext]',
				},
			},
		],
	};

	let prodLoader = {
		loader: 'img-loader',
		options: {
			plugins: [
				require('imagemin-gifsicle')({
					interlaced: true,
				}),
				require('imagemin-mozjpeg')({
					progressive: true,
					arithmetic: false,
				}),
				require('imagemin-optipng')({
					optimizationLevel: 5,
				}),
				require('imagemin-svgo')({
					plugins: [{ convertPathData: false }],
				}),
			],
		},
	};

	return isProduction ? (devConfig = { ...devConfig, use: [...devConfig.use, prodLoader] }) : devConfig;
};

module.exports = {
	imageLoader: imageLoader,
};
