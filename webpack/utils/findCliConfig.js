let cliConfig = null;

try {
	cliConfig = require('../../cli.config.json');
} catch (e) {
	if (e.code !== 'MODULE_NOT_FOUND') console.log(e);
}

module.exports = cliConfig;