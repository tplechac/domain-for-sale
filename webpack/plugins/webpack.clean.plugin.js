const path = require('path');
const settings = require('../webpack.settings.js');
const cliConfig = require('../../cli.config.json');

// Configure Clean webpack
const cleanPluginPaths = (devClean = false) => {
	let cleanSrc = () => {
		switch (cliConfig.projectType) {
			case 'static':
			case 'vue':
				return [
					'scss/01_base/_liferay.scss',
					'scss/01_base/_scaffolding.scss',
					'scss/_core.scss',
					'scss/_custom.scss',
					'scss/_liferay-all.scss',
					'scss/front-liferay.scss',
				];
			case 'components':
				return [
					'scss/01_base/_liferay.scss',
					'scss/01_base/_scaffolding.scss',
					'scss/_custom.scss',
					'scss/_liferay-all.scss',
					'scss/front-liferay.scss'
				];
			case 'liferay':
				return [
					'scss/_core.scss',
					'scss/front.scss'
				];
		}
	};

	return devClean
		? cleanSrc().concat(
				cliConfig.bootstrapVersion === 'modern'
					? 'scss/01_vendors/bootstrap3_init.scss'
					: 'scss/01_vendors/bootstrap_init.scss'
		  )
		: ['assets', 'favicons', '*.*'];
};

const cleanPluginOptions = (devClean = false) => {
	return devClean
		? {
				root: path.join(__dirname, '../../src/'),
				verbose: false,
		  }
		: {
				root: path.join(__dirname, '../../public/'),
		  };
};

module.exports = {
	cleanPluginPaths: cleanPluginPaths,
	cleanPluginOptions: cleanPluginOptions,
};
