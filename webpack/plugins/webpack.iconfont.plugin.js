const iconfontPlugin = () => {
	return {
		filename: './assets/fonts/[name].[hash].[ext]',
		localCSSTemplate: `font-family: {{ fontName }} !important;speak: none;font-style: normal;font-weight: normal;font-variant: normal;text-transform: none;line-height: 1;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;`,
		output: './'
	};
};

module.exports = {
	iconfontPlugin: iconfontPlugin,
};

