const css = require('../loaders/webpack.css.loader.js');
const image = require('../loaders/webpack.image.loader.js');
const raw = require('../loaders/webpack.raw.loader.js');

const path = require('path');

const webpack = require('webpack');
const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin');
const banner = require('../plugins/webpack.banner.plugin.js');
const html = require('../plugins/webpack.html.plugin.js');
const clean = require('../plugins/webpack.clean.plugin.js');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');

const prodConfig = {
	output: {
		filename: path.join('./assets/js/', '[name].[hash].js'),
	},
	mode: 'production',
	devtool: 'source-map',
	optimization: {
		splitChunks: {
			cacheGroups: {
				frontStyles: {
					name: 'frontStyles',
					test: /\.(css|sass|scss)$/,
					chunks: 'all',
				},
			},
		},
	},
	module: {
		rules: [css.cssLoader(true), image.imageLoader(true), raw.rawLoader()],
	},
	plugins: [
		new webpack.optimize.ModuleConcatenationPlugin(),
		new webpack.BannerPlugin(banner.bannerPlugin()),
		new ImageminWebpWebpackPlugin(),
		new FixStyleOnlyEntriesPlugin(),
		new CleanWebpackPlugin(
			clean.cleanPluginPaths(false),
			clean.cleanPluginOptions(false)
		),
		new MiniCssExtractPlugin({
			filename: path.join('./assets/css/', '[name].[hash].css')
		}),
	].concat(html.htmlPlugins()),
};

module.exports = prodConfig;
