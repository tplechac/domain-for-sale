const path = require('path');
const webpack = require('webpack');

const devserver = require('./devserver.config.js');
const settings = require('../webpack.settings.js');

const css = require('../loaders/webpack.css.loader.js');
const image = require('../loaders/webpack.image.loader.js');
const raw = require('../loaders/webpack.raw.loader.js');

const html = require('../plugins/webpack.html.plugin.js');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const stylelint = require('../plugins/webpack.stylelint.plugin.js');
const clean = require('../plugins/webpack.clean.plugin.js');
const StylelintWebpackPlugin = require('stylelint-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const cliConfig = require('../utils/findCliConfig.js');

const devConfig = {
	output: {
		filename: path.join('./assets/js', '[name].[hash].js'),
		publicPath: settings.devServerConfig.public + '/',
	},
	mode: 'development',
	devtool: 'inline-source-map',
	devServer: devserver.devServerConfig,
	module: {
		rules: [css.cssLoader(), image.imageLoader(true), raw.rawLoader()],
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new CleanWebpackPlugin(
			clean.cleanPluginPaths(true),
			clean.cleanPluginOptions(true)
		),
		new StylelintWebpackPlugin(stylelint.stylelintPlugin()),
		new MiniCssExtractPlugin({
			filename: path.join('./assets/css', '[name].[hash].css'),
		}),
	].concat(html.htmlPlugins()),
};

module.exports = devConfig;
